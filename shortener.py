import shelve
import string
import random
import webapp

class URLshortener(webapp.webApp):
    def __init__(self,hostname,port):
        self.urls = shelve.open("urls.db")
        super().__init__(hostname,port)

    def short_url(self,url_lenght):
        url=''.join(random.choice(string.ascii_letters + string.digits) for _ in range(url_lenght))
        return '/'+url

    def parse(self,request):
        metodo = request.split(' ')[0]
        petition = request.split(' ')[1][0:]
        if metodo == "POST":
            body = request.split('url=')[1]
        else:
            body = None
        return metodo, petition, body

    def url_list(self):
        str=''
        for value,key in self.urls.items():
            str=str+key+'->'+value+' , '
        return str

    def process(self,analyzed):
        metodo, petition, body = analyzed
        if metodo=="GET":
            if petition=='/':
                httpCode = "200 OK"
                htmlBody = "<html><head><title>Acortador de URLS</title><meta charset=”UTF-8”>" \
                           "</head><body><h1>Acortador de URLS</h1><h2>"+self.url_list()+"</h2>" \
                           "<form method='POST'><input type='text' placeholder='Escriba la URL a acortar'" \
                           " name='url'</body></html>"
            elif petition in self.urls.keys():
                tmp_str=str(self.urls[petition])
                if tmp_str.startswith("http://"):
                    pag_destino= tmp_str.replace("http://", "")
                else:
                    pag_destino= tmp_str.replace("https://", "")
                pag_inicial= petition
                httpCode = "302 Move Temporarily"
                htmlBody='<html><head><meta http-equiv="refresh" content="5;url'+pag_destino+'"</head>'\
                            '<body><h1> Usted viene del recurso '+pag_inicial+" sera redirido a "+str(pag_destino)+ \
                            '</body></h1></html>'
            else:
                    httpCode = "400 ERROR"
                    htmlBody = "<html><body><h1>Recurso no disponible</h1></body></html>"
        elif metodo=="POST":
            if body== '' or body== None:
                httpCode = "404 ERROR"
                htmlBody = "<html><body><h1>Error found</h1></body></html>"
            else:
                if not (body.startswith("http://")) and not body.startswith("https://"):
                    body="https://"+body

                if not body in  self.urls.values():
                    short_url=self.short_url(12)
                    while short_url in self.urls.keys():#Este bucle se incluye por si da el caso aleatorio que dos urls
                        short_url=self.short_url(12)    #que salen iguales
                    self.urls[short_url]=body
                httpCode="200 OK"
                htmlBody="<html><body><h1>Acortador de URLS</h1><h2>+"+self.url_list()+"</h2>" \
                           "<form method='POST'><input type='text' placeholder='Escriba la URL a acortar'" \
                           " name='url'</body></html>"
        else:
            httpCode = "404 ERROR"
            htmlBody = "<html><body>Error found</body></html>"
        return httpCode,htmlBody


if __name__ == "__main__":
    test=URLshortener("localhost",1234)